import { getTradeQuote } from "../tradeQuote.js";
import { expect } from "chai";
import sinon from "sinon";

describe("Get trade price", async () => {
  it("returns the trade price", async () => {
    const getStockPriceStub = sinon.stub().returns(75);
    const tradePrice = await getTradeQuote("Biocon", 10, getStockPriceStub);
    expect(tradePrice).to.eql(750);
  });
  it("handles invalid stock", async () => {
    const getStockPriceStub = sinon.stub().returns(-1);
    const tradePriceResponse = await getTradeQuote(
      "Biocon",
      10,
      getStockPriceStub
    );
    expect(tradePriceResponse.message).to.eql("Invalid stock");
  });
});
