export const getTradeQuote = async (stock, quantity, getStockPrice) => {
  const stockPrice = await getStockPrice(stock);
  if (stockPrice === -1) {
    return { message: "Invalid stock" };
  }
  return stockPrice * quantity;
};
