import externalCalls from "../stub/externalCalls.js";
import logger from "./logger.js";
export const getTradeQuote = async (
  stock,
  quantity,
  getStockPrice,
  tradeQuoteLogger
) => {
  const stockPrice = await getStockPrice(stock);
  const tradeQuote = stockPrice * quantity;
  tradeQuoteLogger({ stock, quantity, tradeQuote });
  return tradeQuote;
};
