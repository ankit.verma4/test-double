import { getTradeQuote } from "../tradeQuoteLog.js";
import logger from "../logger.js";
import { expect } from "chai";
import sinon from "sinon";

describe("Logs trade quote", async () => {
  it("logs the trade quote", async () => {
    const getStockPriceStub = sinon.stub().returns(75);
    const loggerMock = sinon.stub();
    await getTradeQuote("Biocon", 10, getStockPriceStub, loggerMock);
    expect(loggerMock.calledOnce).to.eql(true);
    expect(
      loggerMock.calledWith({ stock: "Biocon", quantity: 10, tradeQuote: 750 })
    ).to.eql(true);
  });
});
