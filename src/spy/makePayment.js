import axios from "axios";

export const makePayment = (paymentDetails, processPayment, callback) => {
  const paymentDetailsResult = processPayment(paymentDetails);
  const { paymentId, amount } = paymentDetailsResult;
  callback(paymentId, amount);
};

const processPaymentFromStripe = (paymentDetails) => {
  const { paymentId, amount } = axios.post(
    "https://stripe/process-payment",
    paymentDetails
  );
  return {
    paymentId,
    amount,
  };
};
