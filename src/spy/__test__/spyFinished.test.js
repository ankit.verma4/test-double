import { makePayment } from "../makePayment.js";
import sinon from "sinon";
import { expect } from "chai";

describe("Payments", () => {
  it("should process payment and call the callback", () => {
    const processPaymentStub = sinon.stub().returns({
      paymentId: "123",
      amount: 500,
    });
    const spy = sinon.spy();
    makePayment({ amount: 500 }, processPaymentStub, spy);
    expect(spy.calledOnce).to.eql(true);
  });
});
