export const getTradeQuote = async (
  stock,
  quantity,
  getStockPrice,
  token,
  googleReCaptchaVerifyToken
) => {
  const isRecaptchaVerified = await googleReCaptchaVerifyToken(token);
  if (!isRecaptchaVerified) {
    return { message: "recaptcha verification failed" };
  }
  const stockPrice = await getStockPrice(stock);
  const tradeQuote = stockPrice * quantity;
  return tradeQuote;
};
