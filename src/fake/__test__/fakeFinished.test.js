import { expect } from "chai";
import sinon from "sinon";
import { fakeGoogleReCaptchaVerifyToken } from "../fakeGoogleRecaptcha.js";
import { getTradeQuote } from "../tradeQuote.js";

describe("Verify recaptcha", async () => {
  it("recaptcha token is valid", async () => {
    const getStockPriceStub = sinon.stub().returns(75);
    const tradeQuote = await getTradeQuote(
      "Biocon",
      10,
      getStockPriceStub,
      "dummyToken",
      fakeGoogleReCaptchaVerifyToken
    );
    expect(tradeQuote).to.eql(750);
  });
  it("recaptcha token is invalid", async () => {
    const getStockPriceStub = sinon.stub().returns(75);
    const result = await getTradeQuote(
      "Biocon",
      10,
      getStockPriceStub,
      "invalidDummyToken",
      fakeGoogleReCaptchaVerifyToken
    );
    expect(result.message).to.eql("recaptcha verification failed");
  });
});
