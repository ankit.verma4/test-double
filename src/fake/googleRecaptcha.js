import axios from "axios";
import FormData from "form-data";
//method to verify google recaptcha token
export const googleReCaptchaVerifyToken = async (token) => {
  try {
    const formData = new FormData();
    formData.append("secret", config.GOOGLE_RECAPTCHA_V2_SECRET);
    formData.append("response", token);
    const response = await axios.post(
      "https://www.google.com/recaptcha/api/siteverify",
      formData,
      { headers: formData.getHeaders() }
    );
    const success = response.data && response.data.success;
    return success === true;
  } catch (error) {
    console.error(error);
  }
  return false;
};
