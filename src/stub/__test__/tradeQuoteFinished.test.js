import { getTradeQuote } from "../tradeQuote.js";
import externalCalls from "../externalCalls.js";
import { expect } from "chai";
import sinon from "sinon";

describe("Get trade price", async () => {
  it("returns the trade price", async () => {
    const getStockPriceStub = sinon
      .stub(externalCalls, "getStockPriceFromExternalService")
      .returns(75);
    const tradePrice = await getTradeQuote("Biocon", 10);
    expect(tradePrice).to.eql(750);
    getStockPriceStub.restore();
  });
  it("handles invalid stock", async () => {
    const getStockPriceStub = sinon
      .stub(externalCalls, "getStockPriceFromExternalService")
      .returns(-1);
    const tradePriceResponse = await getTradeQuote("Biocon", 10);
    expect(tradePriceResponse.message).to.eql("Invalid stock");
    getStockPriceStub.restore();
  });
});
