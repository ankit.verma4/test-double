import externalCalls from "./externalCalls.js";

export const getTradeQuote = async (stock, quantity) => {
  const stockPrice = await externalCalls.getStockPriceFromExternalService(
    stock
  );
  if (stockPrice === -1) {
    return { message: "Invalid stock" };
  }
  return stockPrice * quantity;
};
