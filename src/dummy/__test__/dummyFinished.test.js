import { getTradeQuote } from "../tradeQuote.js";
import { expect } from "chai";
import sinon from "sinon";

describe("Get trade price", async () => {
  it("returns the trade price", async () => {
    const getStockPriceStub = sinon.stub().returns(75);
    const dummyAddAnalytics = (stock, quantity) => {};
    const tradePrice = await getTradeQuote(
      "Biocon",
      10,
      dummyAddAnalytics,
      getStockPriceStub
    );
    expect(tradePrice).to.eql(750);
  });
});
