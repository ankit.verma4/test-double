const addAnalytics = (stock, quantity) => {
  //A database operation to add analytics
  //db('analytics').save({stock, quantity})
};

export const getTradeQuote = async (
  stock,
  quantity,
  addAnalytics,
  getStockPrice
) => {
  const stockPrice = await getStockPrice(stock);
  addAnalytics(stock, quantity);
  return stockPrice * quantity;
};
