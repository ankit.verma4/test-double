const getStockPriceFromExternalService = async (stock) => {
  const data = await axios(`http://getstockprice/${stock}`);
  return data.price;
};

export default { getStockPriceFromExternalService };
