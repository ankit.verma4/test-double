# test-double

Material for writing tests involving test double with sinon.js

We're going to be learning tests involving test-doubles with
[sinon.js](https://sinonjs.org/).

## Pre-requisites:

You should be familiar with js and testing frameworks chai and mocha. If not, still you can learn the concepts and apply into your own language/framework.

## System Requirements:

You'll need git and node installed.

## Setup:

Run these commands in your terminal to get yourself setup:

```
git clone https://gitlab.com/ankit.verma4/test-double.git
npm install
npm test
```
